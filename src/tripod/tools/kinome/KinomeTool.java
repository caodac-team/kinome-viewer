
package tripod.tools.kinome;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;
import java.net.URL;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import javax.swing.border.Border;

import org.jdesktop.swingx.JXImagePanel;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.*;
import org.jdesktop.swingworker.SwingWorker;
import org.jdesktop.swingx.painter.BusyPainter;

import chemaxon.struc.*;
import chemaxon.formats.*;
import chemaxon.util.MolHandler;
import chemaxon.marvin.beans.MSketchPane;

import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.BasicEventList;

import tripod.ui.kinome.*;
import tripod.ui.base.MolCellRenderer;
import tripod.ui.base.MolCellEditor;
import tripod.ui.base.Grid;
import tripod.ui.base.GridCellAnnotator;

import javax.jnlp.*;

public class KinomeTool extends JFrame implements ActionListener {
    static boolean JNLP = false;

    private static final Logger logger = 
	Logger.getLogger(KinomeTool.class.getName());

    static final Border EMPTY_BORDER = 
	BorderFactory.createEmptyBorder(1,1,1,1);

    static final int DEFAULT_COLUMN_WIDTH = 130;
    static final int DEFAULT_COLUMN_HEIGHT = 150;

    static class BusyPane extends JComponent implements ActionListener {
	javax.swing.Timer timer;
	BusyPainter painter;
	int frame = 0;
	AffineTransform txn = new AffineTransform ();
	int size = 14;
	int count = 0;

	public BusyPane () {
	    setPreferredSize (new Dimension (size+2, size+2));

	    painter = new BusyPainter
		(new RoundRectangle2D.Float(0.f, 0.f,5.f, 2.f, 4.f, 4.f),
		 new Ellipse2D.Float(2.f,2.f,10.0f,10.0f));
	    painter.setTrailLength(6);
	    painter.setPoints(9);
	    painter.setFrame(0);

	    timer = new javax.swing.Timer (125, this);
	}

	public void start () { 
	    if (count++ == 0) {
		timer.start(); 
	    }
	}
	public void stop () { 
	    if (--count == 0) {
		timer.stop(); 
		repaint (); 
	    }
	}
	public void reset () { count = 0; }

	public boolean isBusy () { return timer.isRunning(); }

	@Override
	protected void paintComponent (Graphics g) {
	    if (isBusy ()) {
		Graphics2D g2 = (Graphics2D)g;
		Rectangle clip = g.getClipBounds();
		
		Rectangle r = getBounds ();
		if (r != null && clip.intersects(r)) {
		    int x = r.x + (clip.width - size);
		    int y = r.y + (r.height - size)/2;
		    g2.translate(x, y);
		    painter.paint(g2, this, size, size);
		}
	    }
	}

	public void actionPerformed (ActionEvent e) {
	    frame = (frame+1) % 8;
	    painter.setFrame(frame);
	    repaint ();
	}
    }

    class LoadContentWorker extends SwingWorker<Throwable, Molecule> {
	Object input;
	LoadContentWorker (File file) {
	    this.input = file;
	}

	LoadContentWorker (FileContents file) {
	    this.input = file;
	}

	LoadContentWorker (URL url) {
	    this.input = url;
	}

	@Override
	protected Throwable doInBackground () {
	    SwingUtilities.invokeLater(new Runnable () {
		    public void run () {
			busy.start();
			enableControls (false);
		    }
		});
	    grid.clear();

	    try {
		InputStream is;
		if (input instanceof FileContents) {
		    FileContents file = (FileContents)input;
		    setTitle (file.getName());
		    is = file.getInputStream();
		}
		else if (input instanceof URL) {
		    URL url = (URL)input;
		    is = url.openStream();
		    String name = url.getFile();
		    int pos = name.lastIndexOf("/");
		    if (pos >= 0) {
			name = name.substring(pos+1);
		    }
		    setTitle (name);
		}
		else /*if (input instanceof File)*/ {
		    File file = (File)input;
		    setTitle (file.getName());
		    is = new FileInputStream (file);
		}

		MolImporter mi = new MolImporter (is);
		for (Molecule mol; (mol = mi.read()) != null; ) {
		    grid.addValue(mol);
		    publish (mol);
		}
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Error reading molecules", ex);
		return ex;
	    }
	    return null;
	}

	void setTitle (String name) {
	    if (name != null) {
		KinomeTool.this.setTitle("NCGC Kinome Viewer \u2014 "+name);
	    }
	    else {
		KinomeTool.this.setTitle("NCGC Kinome Viewer");
	    }
	}

	@Override
	protected void process (Molecule... mols) {
	}

	@Override
	protected void done () {
	    busy.stop();
	    try {
		Throwable t = get ();
		if (t != null) {
		    JOptionPane.showMessageDialog
			(KinomeTool.this,  t.getMessage(), "Error", 
			 JOptionPane.ERROR_MESSAGE);
		    logger.log(Level.SEVERE, "Load file error", t);
		}
		else {
		    enableControls (true);
		}
	    }
	    catch (Exception ex) {
		JOptionPane.showMessageDialog
		    (KinomeTool.this,  ex.getMessage(), "Error", 
		     JOptionPane.ERROR_MESSAGE);
		logger.log(Level.SEVERE, "Load file error", ex);
	    }
	}
    }

    class TabMolCellRenderer extends JPanel implements TableCellRenderer {
	MolCellRenderer mcr = new MolCellRenderer ();
	JLabel label = new JLabel ();
	TabMolCellRenderer () {
	    setLayout (new BorderLayout ());
	    setBackground (Color.white);

	    add (mcr);
	    add (label, BorderLayout.NORTH);
	    label.setHorizontalTextPosition(SwingConstants.CENTER);
	    label.setHorizontalAlignment(SwingConstants.CENTER);
	}
	
	public Component getTableCellRendererComponent 
	    (JTable table, Object value, boolean selected, 
	     boolean focus, int row, int col) {
	    if (value != null) {
		if (value instanceof Molecule) {
		    mcr.setValue(value, selected);
		    label.setText(((Molecule)value).getName());
		}
		else {
		    try {
			MolHandler mh = new MolHandler (value.toString());
			Molecule mol = mh.getMolecule();
			mcr.setValue(mol, selected);
			label.setText(mol.getName());
		    }
		    catch (Exception ex) {
			mcr.setValue(null);
		    }
		}
	    }
	    else {
		label.setText(null);
		mcr.setValue(null);
	    }
	    return this;
	}
    }

    class KinomeTableModel extends AbstractTableModel 
	implements ListEventListener<Molecule> {

	EventList<Molecule> mols = new BasicEventList<Molecule>();
	Map<Molecule, KinomeDataset> datasets = 
	    new HashMap<Molecule, KinomeDataset>();

	KinomeTableModel () {
	    mols.addListEventListener(this);
	}

	public void add (Molecule m) {
	    mols.getReadWriteLock().writeLock().lock();
	    try {
		mols.add(m);
	    }
	    finally {
		mols.getReadWriteLock().writeLock().unlock();
	    }
	}

	KinomeDataset createDataset (Molecule m) {
	    KinomeDataset kds = new KinomeDataset (m.getName());
	    for (int i = 0; i < m.getPropertyCount(); ++i) {
		String kinase = m.getPropertyKey(i);
		String value = m.getProperty(kinase);
		try {
		    double x = Double.parseDouble(value);
		    if (!kds.setValue(kinase, x)) {
			logger.info(m.getName()+": \""+kinase
				    +"\" is not a recognized kinase!");
		    }
		}
		catch (NumberFormatException ex) {
		    // ignore
		}
	    }
	    return kds;
	}

	public void clear () {
	    mols.getReadWriteLock().writeLock().lock();
	    try {
		mols.clear();
		datasets.clear();
	    }
	    finally {
		mols.getReadWriteLock().writeLock().unlock();
	    }
	}

	public int getRowCount () { return mols.size(); }
	public int getColumnCount () { return mols.size(); }
	public String getColumnName (int col) {
	    Molecule mol = mols.get(col);
	    return mol.toFormat("mol");
	}
	public Class getColumnClass (int col) {
	    return KinomeDataset[].class;
	}
	public Object getValueAt (int row, int col) {
	    mols.getReadWriteLock().readLock().lock();
	    try {
		KinomeDataset[] kds;
		if (row == col) { // diagonal... 
		    kds = new KinomeDataset[1];
		    kds[0] = datasets.get(mols.get(row));
		}
		else {
		    kds = new KinomeDataset[2];
		    kds[0] = datasets.get(mols.get(row));
		    kds[1] = datasets.get(mols.get(col));
		}
		return kds;
	    }
	    finally {
		mols.getReadWriteLock().readLock().unlock();
	    }
	}
	public void setValueAt (Object value, int row, int col) {
	    // do nothing...
	}

	public boolean isCellEditable (int row, int col) { return true; }
	public Molecule getMol (int pos) { 
	    mols.getReadWriteLock().readLock().lock();
	    try {
		return mols.get(pos); 
	    }
	    finally {
		mols.getReadWriteLock().readLock().unlock();
	    }
	}
	public void setMol (int pos, Molecule m) { 
	    mols.getReadWriteLock().writeLock().lock();
	    try {
		KinomeDataset kds = datasets.get(mols.get(pos));
		mols.set(pos, m); 
		datasets.put(m, kds);
		fireTableDataChanged ();
	    }
	    finally {
		mols.getReadWriteLock().writeLock().unlock();
	    }
	}

	public void listChanged (ListEvent<Molecule> e) {
	    EventList<Molecule> source = e.getSourceList();
	    int inserts = 0, deletes = 0;
	    while (e.next()) {
		int type = e.getType();
		if (type == ListEvent.INSERT) {
		    Molecule m = source.get(e.getIndex());
		    datasets.put(m, createDataset (m));
		}
		else if (type == ListEvent.DELETE) {
		}
		else { // update
		}
	    }
	    fireTableStructureChanged ();
	}
    }

    class RowTableModel extends AbstractTableModel 
	implements TableModelListener {
	KinomeTableModel ktm;
	RowTableModel (KinomeTableModel ktm) {
	    this.ktm = ktm;
	    ktm.addTableModelListener(this);
	}

	public void tableChanged (TableModelEvent e) {
	    fireTableStructureChanged ();
	}

	public int getRowCount () { return ktm.getRowCount(); }
	public int getColumnCount () { return 1; }
	public Class getColumnClass (int col) { return Molecule.class; }
	public Object getValueAt (int row, int col) {
	    return ktm.getMol(row);
	}
	public void setValueAt (Object value, int row, int col) {
	    if (value instanceof Molecule) {
		ktm.setMol(row, (Molecule)value);
	    }
	}
	public boolean isCellEditable (int row, int col) { return true; }
	public String getColumnName (int column) { 
	    // a hack to get the row header to have proper size... sigh
	    return "<html><pre>                 "; 
	}
    }

    static class KinomeTable extends JXTable {
	private ColumnFactory cf = new ColumnFactory () {
		@Override
		public void configureColumnWidths (JXTable table,
						   TableColumnExt column) {
		    // do nothing...
		}
	    };

	public KinomeTable () {
	    setColumnFactory (cf);
	    setSortable (false);
	}

	public KinomeTable (TableModel model) {
	    super (model);
	    setColumnFactory (cf);	    
	    setSortable (false);
	}

	@Override
	public void createDefaultColumnsFromModel () {
	    TableColumnModel model = getColumnModel ();
	    Map<Integer, TableColumn> columns = 
		new HashMap<Integer, TableColumn>();
	    for (int c = 0; c < model.getColumnCount(); ++c) {
		TableColumn tc = model.getColumn(c);
		columns.put(tc.getModelIndex(), tc);
	    }
	    
	    TableModel tm = getModel ();
	    for (int c = 0; c < tm.getColumnCount(); ++c) {
		TableColumnExt tc = (TableColumnExt)columns.get(c);
		if (tc == null) {
		    tc = new TableColumnExt (c, DEFAULT_COLUMN_WIDTH);
		    tc.setHeaderValue(tm.getColumnName(c));
		    model.addColumn(tc);
		}
		else {
		    // make sure the column header is update
		    tc.setHeaderValue(tm.getColumnName(c));
		}
	    }
	    
	    for (Map.Entry<Integer, TableColumn> me : 
		     columns.entrySet()) {
		if (me.getKey() < tm.getColumnCount()) {
		}
		else {
		    // remove
		    model.removeColumn(me.getValue());
		}
	    }
	    JTableHeader header = getTableHeader ();
	    if (header != null) {
		header.repaint();
	    }
	}
    }

    JTable datatab;
    JButton addBtn, clearBtn;
    JSlider rowSlider, colSlider;
    KinomeCellRenderer kinome;
    Grid grid;
    JFileChooser chooser;
    BusyPane busy;

    public KinomeTool () {
	initUI ();
    }

    protected void initUI () {
	if (!JNLP) {
	    chooser = new JFileChooser (".");
	}

	setJMenuBar (createMenuBar ());

	JSplitPane split = new JSplitPane ();
	split.setDividerSize(2);
	split.setResizeWeight(0.35);
	split.setLeftComponent(createContentPane ());
	split.setRightComponent(createViewPane ());

	JPanel pane = new JPanel (new BorderLayout (0, 5));
	pane.add(split);
	pane.setBorder(EMPTY_BORDER);
	
	getContentPane().add(pane);
	pack ();

	setTitle ("NCGC Kinome Viewer");
    }

    JMenuBar createMenuBar () {
	JMenu file = new JMenu ("File");
	JMenuItem item;
	file.add(item = new JMenuItem ("Open"));
	item.setToolTipText("Open file");
	item.addActionListener(this);
	file.addSeparator();
	file.add(item = new JMenuItem ("Save Image"));
	item.setToolTipText("Save kinome image");
	item.addActionListener(this);
	file.addSeparator();
	file.add(item = new JMenuItem ("Demo \u2014 Ambit"));
	item.setToolTipText("Load Ambit data");
	item.addActionListener(this);
	file.add(item = new JMenuItem ("Demo \u2014 ChEMBL"));
	item.setToolTipText("Load kinase drug data from ChEMBL");
	item.addActionListener(this);
	file.add(item = new JMenuItem ("Demo \u2014 Abbott"));
	item.setToolTipText("Load Abbott kinase data from "
			    +"the paper \"Navigating the kinome\"");
	item.addActionListener(this);	
	file.addSeparator();
	file.add(item = new JMenuItem ("Quit"));
	item.addActionListener(this);

	JMenuBar menubar = new JMenuBar ();
	menubar.add(file);
	return menubar;
    }

    Component createContentPane () {
	grid = new Grid ();
	grid.setCellRenderer(new MolCellRenderer ());
	MolCellEditor mce = new MolCellEditor ();
	mce.setEnableAction(false); // security exception in unsigned webstart
	grid.setCellEditor(mce);
	grid.setCellAnnotator(new GridCellAnnotator () {
		public String getGridCellBadgeAnnotation 
		    (Grid g, Object value, int cell) {
		    return null;
		}
		public String getGridCellAnnotation 
		    (Grid g, Object value, int cell) {
		    if (value != null) {
			Molecule core = (Molecule)value;
			return core.getName();
		    }
		    return null;
		}
	    });

	JPanel pane = new JPanel (new BorderLayout (0, 5));
	pane.setBorder(EMPTY_BORDER);
	pane.add(new JScrollPane (grid));

	JPanel bp = new JPanel (new GridLayout (1, 2, 2, 0));
	bp.add(addBtn = new JButton ("Add"));
	addBtn.setToolTipText("Add selected structure(s) to table");
	addBtn.addActionListener(this);
	bp.add(clearBtn = new JButton ("Clear"));
	clearBtn.setToolTipText("Clear table");
	clearBtn.addActionListener(this);
	
	JPanel bbp = new JPanel ();
	bbp.add(bp);

	JPanel bbpp = new JPanel (new BorderLayout ());
	bbpp.add(busy = new BusyPane (), BorderLayout.WEST);
	bbpp.add(bbp);

	pane.add(bbpp, BorderLayout.SOUTH);	
	enableControls (false);

	kinome = new KinomeCellRenderer ();
	
	JSplitPane split = new JSplitPane (JSplitPane.VERTICAL_SPLIT);
	split.setDividerSize(2);
	split.setResizeWeight(0.5);
	split.setLeftComponent(kinome);
	split.setRightComponent(pane);

	return split;
    }

    void enableControls (boolean enabled) {
	addBtn.setEnabled(enabled);
	clearBtn.setEnabled(enabled);
    }

    Component createViewPane () {
	KinomeTableModel ktm = new KinomeTableModel ();
	datatab = new KinomeTable (ktm);/* {
		@Override
		public Component prepareEditor 
		    (TableCellEditor editor, int row, int col) {
		    logger.info("editing cell "+row+" "+col);
		    return super.prepareEditor(editor, row, col);
		}
	    };
				    */
	datatab.setShowHorizontalLines(false);
	datatab.setShowVerticalLines(false);
	datatab.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);
	datatab.setDefaultRenderer
	    (KinomeDataset[].class, new KinomeCellRenderer ());
	datatab.setDefaultEditor
	    (KinomeDataset[].class, new KinomeCellEditor ());
	datatab.setCellSelectionEnabled(true);
	datatab.setRowHeight(DEFAULT_COLUMN_HEIGHT);
	TabMolCellRenderer header = new TabMolCellRenderer ();
	header.setPreferredSize
	    (new Dimension (DEFAULT_COLUMN_WIDTH, DEFAULT_COLUMN_HEIGHT));
	datatab.getTableHeader().setDefaultRenderer(header);
	datatab.getSelectionModel()
	    .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	datatab.addMouseListener(new MouseAdapter () {
		public void mousePressed (MouseEvent e) {
		    selectGesture (e);
		}
		public void mouseClicked (MouseEvent e) {
		    selectGesture (e);
		}
	    });


	JTable rowtab = new JXTable (new RowTableModel (ktm));
	rowtab.getTableHeader().setResizingAllowed(false);
	rowtab.setShowHorizontalLines(false);
	rowtab.setShowVerticalLines(false);
	rowtab.setDefaultEditor(Molecule.class, new  MolCellEditor ());
	rowtab.setDefaultRenderer(Molecule.class, new TabMolCellRenderer ());
	rowtab.setRowHeight(datatab.getRowHeight());

	final JPanel logo = new JPanel ();
	logo.setBackground(Color.white);
	logo.setPreferredSize
	    (new Dimension (DEFAULT_COLUMN_WIDTH, DEFAULT_COLUMN_HEIGHT));
	rowtab.getTableHeader().setDefaultRenderer
	    (new TableCellRenderer () {
		    public Component getTableCellRendererComponent
			(JTable table, Object value, boolean isSelected, 
			 boolean hasFocus, int row, int column) {
			return logo;
		    }
		});

	JScrollPane sp = new JScrollPane ();
	sp.setViewportView(datatab);
	sp.setCorner(JScrollPane.UPPER_LEFT_CORNER, rowtab.getTableHeader());
	sp.setRowHeaderView(rowtab);

	return sp;
    }

    void selectGesture (MouseEvent e) {
	Point pt = e.getPoint();
	int col = datatab.columnAtPoint(pt);
	int row = datatab.rowAtPoint(pt);
	//logger.info("row="+row+" col="+col);
	if (col >= 0 && row >= 0) {
	    datatab.setColumnSelectionInterval(col, col);
	    datatab.setRowSelectionInterval(row, row);
	    Object value = datatab.getValueAt(row, col);
	    kinome.setValue(value);
	}
    }

    public void actionPerformed (ActionEvent e) {
	String cmd = e.getActionCommand();
	if (cmd.equalsIgnoreCase("quit")) {
	    System.exit(0);
	}
	else if (cmd.startsWith("Save")) {
	    save ();
	}
	else if (cmd.startsWith("Demo")) {
	    if (cmd.indexOf("Ambit") >= 0) {
		demoAmbit ();
	    }
	    else if (cmd.indexOf("ChEMBL") >= 0) {
		demoChEMBL ();
	    }
	    else {
		demoAbbott ();
	    }
	}
	else if (cmd.equalsIgnoreCase("open")) {
	    open ();
	}
	else if (cmd.equalsIgnoreCase("add")) {
	    add ();
	}
	else if (cmd.equalsIgnoreCase("clear")) {
	    clear ();
	}
    }

    void demoAmbit () {
	new LoadContentWorker 
	    (KinomeTool.class.getResource
	     ("resources/ambit.sdf")).execute();
    }

    void demoChEMBL () {
	new LoadContentWorker 
	    (KinomeTool.class.getResource
	     ("resources/ChemblKinaseDrugs.sdf")).execute();
    }

    void demoAbbott () {
	new LoadContentWorker 
	    (KinomeTool.class.getResource
	     ("resources/nchembio.530-S2.sdf")).execute();
    }

    void add () {
	int[] cells = grid.getSelectedCells();
	if (cells == null || cells.length == 0) {
	    JOptionPane.showMessageDialog
		(this, "No structures selected!", "Warning", 
		 JOptionPane.WARNING_MESSAGE);
	    return;
	}

	KinomeTableModel ktm = (KinomeTableModel)datatab.getModel();
	for (int i = 0; i < cells.length; ++i) {
	    ktm.add((Molecule)grid.getValueAt(cells[i]));
	}
    }

    void save () {
	if (JNLP) {
	    try {
		final FileSaveService fss = 
		    (FileSaveService)ServiceManager.lookup
		    ("javax.jnlp.FileSaveService");
		
		// write an empty byte to the file
		FileContents file = fss.saveFileDialog
		    (null, null, new ByteArrayInputStream
		     ("".getBytes("UTF-8")), null);
		if (file == null || !file.canWrite()) {
		    JOptionPane.showMessageDialog
			(this, "Can't open file for writing", "Error", 
			 JOptionPane.ERROR_MESSAGE);
		}
		else {
		    file.setMaxLength(5*1024*1024);// 5 mb max
		    OutputStream os = file.getOutputStream(true);
		    kinome.exportImage(os);
		    os.close();
		}
	    }
	    catch (Exception ex) {
		JOptionPane.showMessageDialog
		    (this, ex.getMessage(), "Error", 
			 JOptionPane.ERROR_MESSAGE);
	    }
	}
	else {
	    chooser.setDialogTitle("Please select output file");
	    if (JFileChooser.APPROVE_OPTION 
		== chooser.showSaveDialog(this)) {
		File file = chooser.getSelectedFile();
		if (file.exists()) {
		    int ans = JOptionPane.showConfirmDialog
			(this, "File \""+file.getName()
			 +"\" exits; overwrite?", "Warning", 
			 JOptionPane.OK_CANCEL_OPTION, 
			 JOptionPane.QUESTION_MESSAGE);
		    if (ans == JOptionPane.CANCEL_OPTION) {
			return;
		    }
		}

		try {
		    FileOutputStream fos = new FileOutputStream (file);
		    kinome.exportImage(fos);
		    fos.close();
		}
		catch (Exception ex) {
		    JOptionPane.showMessageDialog
			(this, ex.getMessage(), "Error", 
			 JOptionPane.ERROR_MESSAGE);
		}
	    }
	}
    }

    void clear () {
	KinomeTableModel ktm = (KinomeTableModel)datatab.getModel();
	ktm.clear();
    }

    void open () {
	if (JNLP) {
	    try {
		FileOpenService fos = 
		    (FileOpenService)ServiceManager.lookup
		    ("javax.jnlp.FileOpenService");
		FileContents file = fos.openFileDialog
		    (".", new String[]{"sdf","mol","mol2","smi","smiles",
				       "cml","sd"});
		if (file == null) {
		    JOptionPane.showMessageDialog
			(this, "No file(s) selected!", "Message", 
			 JOptionPane.PLAIN_MESSAGE);
		}
		else {
		    new LoadContentWorker(file).execute();
		}
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Can't open file dialog", ex);
		JOptionPane.showMessageDialog
		    (this, ex.getMessage(), "Error", 
		     JOptionPane.ERROR_MESSAGE);
	    }
	}
	else {
	    if (JFileChooser.APPROVE_OPTION == chooser.showOpenDialog(this)) {
		new LoadContentWorker(chooser.getSelectedFile()).execute();
	    }
	}
    }

    public static void launch (String[] argv) throws Exception {
	SwingUtilities.invokeLater(new Runnable () {
		public void run () {
		    KinomeTool kt = new KinomeTool ();
		    kt.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    kt.setSize(800, 600);
		    kt.setVisible(true);
		}
	    });
    }

    public static void main (String[] argv) throws Exception {
	launch (argv);
    }

    public static class JnlpLaunch {
	public static void main (final String[] argv) throws Exception {
	    JNLP = true;
	    launch (argv);
	}
    }
}
