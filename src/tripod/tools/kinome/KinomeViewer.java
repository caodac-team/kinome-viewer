package tripod.tools.kinome;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import com.jgoodies.looks.plastic.theme.*;

import tripod.ui.kinome.*;

public class KinomeViewer extends JFrame 
    implements ActionListener, PropertyChangeListener {

    private static final Logger logger = 
	Logger.getLogger(KinomeViewer.class.getName());

    KinomePanel kinome = new KinomePanel ();
    JFileChooser chooser = new JFileChooser (".");
    JMenu optionMenu;

    public KinomeViewer () {
        initUI ();
    }

    protected void initUI () {
        setJMenuBar (createMenuBar ());

	JPanel pane = new JPanel (new BorderLayout (0, 5));
        kinome.setBackground(Color.white);
        kinome.addPropertyChangeListener(this);
	pane.add(kinome);
	pane.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
	
	getContentPane().add(pane);
	pack ();

        setDefaultCloseOperation (EXIT_ON_CLOSE);
        setTitle ("NCGC Kinome Viewer");
    }

    JMenuBar createMenuBar () {
	JMenu file = new JMenu ("File");
	JMenuItem item;

        file.add(item = new JMenuItem ("Import"));
        item.setToolTipText("Import data");
        item.addActionListener(this);

        file.add(item = new JMenuItem ("Export"));
        item.setToolTipText("Export kinome image");
        item.addActionListener(this);

        file.addSeparator();
        file.add(item = new JMenuItem ("Quit"));
        item.addActionListener(this);

        optionMenu = new JMenu ("Options");
        optionMenu.add(item = new JCheckBoxMenuItem ("Border"));
        item.setSelected(true);
        item.setToolTipText("Show border");
        item.addActionListener(this);

        optionMenu.add(item = new JMenuItem ("Clear"));
        item.setToolTipText("Clear all datasets in the kinome");
        item.setActionCommand("clearAll");
        item.addActionListener(this);

        optionMenu.add(item = new JMenuItem ("Clear labels"));
        item.setActionCommand("clearLabels");
        item.setToolTipText("Clear kinase labels");
        item.addActionListener(this);
        optionMenu.addSeparator();

        JMenuBar menubar = new JMenuBar ();
        menubar.add(file);
        menubar.add(optionMenu);

        return menubar;
    }

    public void actionPerformed (ActionEvent e) {
        AbstractButton ab = (AbstractButton)e.getSource();
        String cmd = e.getActionCommand();

        if ("export".equalsIgnoreCase(cmd)) {
            export ();
        }
        else if ("import".equalsIgnoreCase(cmd)) {
            ImportWizard wizard = new ImportWizard (this);
            wizard.setVisible(true);
        }
        else if ("border".equalsIgnoreCase(cmd)) {
            kinome.setPaintBorder(ab.isSelected());
        }
        else if ("clearAll".equalsIgnoreCase(cmd)) {
            kinome.clear();
            // remove all menu items
            Component[] comps = optionMenu.getMenuComponents();
            for (int i = comps.length; --i >= 0; ) {
                Component c = comps[i];
                if (c instanceof JSeparator) {
                    break;
                }
                optionMenu.remove(c);
            }
        }
        else if ("clearLabels".equalsIgnoreCase(cmd)) {
            kinome.clearLabels();
        }
        else if ("quit".equalsIgnoreCase(cmd)) {
            System.exit(0);
        }
        else if (ab instanceof JCheckBoxMenuItem) {
            // dataset toggle
            kinome.setVisible(ab.getText(), ab.isSelected());
        }
    }

    public void propertyChange (PropertyChangeEvent e) {
        String prop = e.getPropertyName();
        if (prop.equals(KinomePanel.PROP_DATASET_ADDED)) {
            KinomeDataset kd = (KinomeDataset)e.getOldValue();
            KinomeRendererFactory krf = (KinomeRendererFactory)e.getNewValue();
            logger.info("## dataset added \""+kd.getName()+"\"");
            JCheckBoxMenuItem item = new JCheckBoxMenuItem (kd.getName());
            item.setToolTipText("Show dataset \""+kd.getName()+"\"");
            item.setSelected(true);
            item.addActionListener(this);
            optionMenu.add(item);
        }
    }

    public void export () {
        chooser.setDialogTitle("Please select output file");
        if (JFileChooser.APPROVE_OPTION 
            == chooser.showSaveDialog(this)) {
            File file = chooser.getSelectedFile();
            if (file.exists()) {
                int ans = JOptionPane.showConfirmDialog
                    (this, "File \""+file.getName()
                     +"\" exits; overwrite?", "Warning", 
                     JOptionPane.OK_CANCEL_OPTION, 
                     JOptionPane.QUESTION_MESSAGE);
                if (ans == JOptionPane.CANCEL_OPTION) {
                    return;
                }
            }
            
            try {
                FileOutputStream fos = new FileOutputStream (file);
                kinome.exportImage(fos);
                fos.close();
            }
            catch (Exception ex) {
                JOptionPane.showMessageDialog
                    (this, ex.getMessage(), "Error", 
                     JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public KinomePanel getKinomePanel () { return kinome; }

    static String[] tokenizer (String line, char delim) {
	java.util.List<String> toks = new ArrayList<String>();

	int len = line.length(), parity = 0;
	StringBuilder curtok = new StringBuilder ();
	for (int i = 0; i < len; ++i) {
	    char ch = line.charAt(i);
	    if (ch == '"') {
		parity ^= 1;
	    }
	    if (ch == delim) {
		if (parity == 0) {
		    String tok = null;
		    if (curtok.length() > 0) {
			tok = curtok.toString();
		    }
		    toks.add(tok);
		    curtok.setLength(0);
		}
		else {
		    curtok.append(ch);
		}
	    }
	    else if (ch != '"') {
		curtok.append(ch);
	    }
	}

	if (curtok.length() > 0) {
	    toks.add(curtok.toString());
	}

	return toks.toArray(new String[0]);
    }

    static void launch (String[] argv) {
	Plastic3DLookAndFeel.setPlasticTheme(new DesertBlue ());
	try {
	    UIManager.setLookAndFeel(new Plastic3DLookAndFeel());
	} catch (Exception e) {}

        KinomeViewer kv = new KinomeViewer ();
        kv.setSize(400, 600);
        kv.setVisible(true);
    }

    public static void main (final String[] argv) throws Exception {
        SwingUtilities.invokeLater(new Runnable () {
                public void run () {
                    launch (argv);
                }
            });
    }
}
