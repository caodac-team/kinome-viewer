
package tripod.tools.kinome;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import org.jdesktop.swingx.JXErrorPane;

import com.jgoodies.forms.layout.*;
import com.jgoodies.forms.builder.PanelBuilder;

import tripod.ui.kinome.*;


public class StepConfig extends StepCommon implements ActionListener {
    private static final Logger logger = Logger.getLogger
	(StepConfig.class.getName());

    public static final String NAME = "Kinome Configuration";
    public static final String DESC = 
        "Configure data for the kinome. For each dataset, please specify the "
        +"color and activity ranges. Note that an activity range is defined "
        +"by the upper bound <i>U<sub>k</sub></i> such that, for a given "
        +"activity <i>x</i>, the smallest <i>k</i> that satifies "
        +"<i>x</i> &le; <i>U<sub>k</sub></i> is used, starting from "
        +"<i>Tiny</i> to <i>Large</i> inorder.";


    static final Color[] defaultColors = {
        new Color (204,0,204).darker(),
        new Color (153,153,0).darker(),
        new Color (0,204,204).darker(),
        Color.blue,
        Color.green.darker(),
        Color.red,
        new Color (102,0,102).darker()
    };

    ArrayList<JComponent[]> rows = new ArrayList<JComponent[]>();

    PanelBuilder form;
    CellConstraints cc;

    public StepConfig () {
	super (NAME, DESC);
    }

    @Override
    protected void initUI () {
        JPanel pane = new JPanel (new BorderLayout (0, 2));

        // Dataset Color Tiny Small Medium Large
        FormLayout layout = new FormLayout
            ("c:p,3dlu,c:p,3dlu,c:p,3dlu,c:p,3dlu,c:p,3dlu,c:p");
        form = new PanelBuilder (layout);
        cc = new CellConstraints ();

        setupRowHeader ();
        form.getPanel().setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
        pane.add(new JScrollPane (form.getPanel()));
        
        setLayout (new BorderLayout ());
        add (pane);
    }

    void setupRowHeader () {
        DefaultKinomeRendererFactory fac = 
            new DefaultKinomeRendererFactory (Color.black);

        form.appendRow(new RowSpec ("pref"));
        form.add(new JLabel ("Dataset"), cc.xy(1,1));
        form.add(new JLabel ("Color"), cc.xy(3, 1));
        form.add(new JLabel ("Tiny", fac.tinyIcon(), JLabel.CENTER),
                 cc.xy(5, 1));
        form.add(new JLabel ("Small", fac.smallIcon(), JLabel.CENTER), 
                 cc.xy(7, 1));
        form.add(new JLabel ("Medium", fac.mediumIcon(), JLabel.CENTER),
                 cc.xy(9, 1));
        form.add(new JLabel ("Large", fac.largeIcon(), JLabel.CENTER),
                 cc.xy(11, 1));
        form.appendRow(new RowSpec ("3dlu"));
        form.appendRow(new RowSpec ("pref"));
        form.addSeparator("", cc.xyw(1, 3, 11));
    }

    JComponent[] addRow () {
        JComponent[] comps = new JComponent[6];
        form.appendRow(new RowSpec ("3dlu"));

	int y = form.getRowCount() + 1;
	form.appendRow(new RowSpec ("pref"));

        comps[0] = new JTextField (13);
        comps[0].setToolTipText("Dataset label");
        ((JTextField)comps[0]).setEditable(false);
        JButton btn = new JButton ();
        btn.setBackground(defaultColors[y%defaultColors.length]);
        btn.setActionCommand("color");
        btn.setFocusPainted(false);
        btn.addActionListener(this);

        comps[1] = btn;
        comps[1].setToolTipText("Click to edit dataset color");
        comps[2] = new JTextField (5);
        comps[2].setToolTipText("Define range (if any) for tiny bubble");
        comps[3] = new JTextField (5);
        comps[3].setToolTipText("Define range (if any) for small bubble");
        comps[4] = new JTextField (5);
        comps[4].setToolTipText("Define range (if any) for medium bubble");
        comps[5] = new JTextField (5);
        comps[5].setToolTipText("Define range (if any) for large bubble");
        btn.setPreferredSize(comps[2].getPreferredSize());

        for (int i = 0, j = 1; i < comps.length; ++i, j+= 2) {
            // every component reference its own row
            comps[i].putClientProperty("Row", comps);
            form.add(comps[i], cc.xy(j, y));
        }
        rows.add(comps);

        return comps;
    }


    public void actionPerformed (ActionEvent e) {
	String cmd = e.getActionCommand();
        if ("color".equalsIgnoreCase(cmd)) {
            JComponent source = (JComponent)e.getSource();
            JComponent[] row = (JComponent[])source.getClientProperty("Row");
            JColorChooser chooser = getColorChooser ();
            Color color = chooser.showDialog
                (this, "Select color for dataset "
                 +((JTextField)row[0]).getText(), source.getBackground());
            if (color != null) {
                source.setBackground(color);
            }
        }
    }


    @Override
    public void prepare () {
        DataImportModel model = 
            (DataImportModel)getModel().get("DataImportModel");
        
        for (JComponent[] r : rows) {
            for (JComponent c : r) {
                form.getPanel().remove(c);
            }
        }
        rows.clear();

        //logger.info("rows: "+form.getRowCount());
        // remove all rows up to the header
        for (int i = form.getRowCount(); i >= 4; --i) {
            form.getLayout().removeRow(i);
        }

        // get unique values for dataset
        Map dsets = model.getValues(DataLabel.Dataset);
        if (dsets == null) {
            // only one dataset
            JComponent[] comps = addRow ();
            ((JTextField)comps[0]).setText("Default");

        }
        else {
            for (Object dv : dsets.keySet()) {
                JComponent[] comps = addRow ();
                ((JTextField)comps[0]).setText(dv.toString());
            }
        }
    }

    @Override
    public void applyState () throws InvalidStateException {
        ArrayList<RenderedKinomeDataset> datasets = 
            new ArrayList<RenderedKinomeDataset>();

        DataImportModel model = 
            (DataImportModel)getModel().get("DataImportModel");
        for (JComponent[] r : rows) {
            // validate each row to have at least one activity range
            RenderedKinomeDataset rkd = null;
            for (int i = 2; i < r.length; ++i) {
                try {
                    double x = Double.parseDouble
                        (((JTextField)r[i]).getText());
                    if (rkd == null) {
                        rkd = new RenderedKinomeDataset (r[1].getBackground());
                    }

                    switch (i) {
                    case 0: // dataset name
                    case 1: // color
                        break;
                    case 2: // tiny
                        rkd.setTiny(x); 
                        break;
                    case 3: // small
                        rkd.setSmall(x); 
                        break;
                    case 4: // medium
                        rkd.setMedium(x);
                        break;
                    case 5: // large
                        rkd.setLarge(x);
                        break;
                    }
                }
                catch (NumberFormatException ex) {
                    ((JTextField)r[i]).setText(null);
                }
            }

            if (rkd == null) {
                throw new InvalidStateException
                    ("Dataset "+((JTextField)r[0]).getText()
                     +" has no activity ranges defined!");
            }

            KinomeDataset kd = model.getDataset(((JTextField)r[0]).getText());
            rkd.setDataset(kd);

            datasets.add(rkd);
        }

        getModel().put("RenderedDatasets", datasets);

        KinomePanel kp = (KinomePanel)getModel().get("KinomePanel");
        for (RenderedKinomeDataset rkd : datasets) {
            kp.add(rkd.getDataset(), rkd);
        }
    }

    public static void main (String[] argv) throws Exception {
        
    }
}
