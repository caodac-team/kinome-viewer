// $Id$

package tripod.ui.util;

import java.io.IOException;
import java.net.URL;
import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Random;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.Border;

import org.jdesktop.swingx.painter.BusyPainter;


/**
 * UI-related utilities
 */
public class UIUtil {
    static public final Border BORDER = 
	BorderFactory.createLineBorder(new Color (0xa5a5a5));

    static public final String CSS = 
	"<html><body style=\"font: 10px 'Lucida Grande',"
	+"Verdana, Helvetica, Arial, Geneva, sans-serif; color: #333;\">";

    public static class BouncingHeads 
	extends JComponent implements ActionListener {
	javax.swing.Timer animator;
	
	static final ImageIcon icon[] = new ImageIcon[] {
	    createImageIcon ("resources/chris4.png"),
	    createImageIcon ("resources/adam.png"),
	    createImageIcon ("resources/noel.png"),
	    createImageIcon ("resources/paul.png"),
	    createImageIcon ("resources/ruili.png"),
	    createImageIcon ("resources/tbone.png"),
	    createImageIcon ("resources/yuhong.png")
	};

	static final Color START_COLOR = new Color (0x474747);
	static final Color END_COLOR = new Color (0x6a6a6a);


	int tmpScale;

	final static int numImages = icon.length;

	double x[] = new double[numImages];
	double y[] = new double[numImages];

	int xh[] = new int[numImages];
	int yh[] = new int[numImages];

	double scale[] = new double[numImages];

	public BouncingHeads () {
	    //setBackground(Color.black);

	    /*
	    for(int i = 0; i < 6; i++) {
		x[i] = (double) rand.nextInt(500);
		y[i] = (double) rand.nextInt(500);
	    }
	    */
	}
	
	public void go () {
	    animator = new javax.swing.Timer(100/*22 + 22 + 22*/, this);
	    animator.start();
	}

	protected void paintGradientBackground 
	    (Graphics g, Rectangle r, Color from, Color to) {
	    Graphics2D g2 = (Graphics2D)g;

	    /*
	      GradientPaint gp = new GradientPaint 
		(r.x, r.y, from, 1, r.y+r.height, to);
	    g2.setPaint(gp);
	    */
	    g2.setPaint(from);
	    g2.fillRect(r.x, r.y, r.width, r.height);
	}

	public void paint(Graphics g) {
	    paintGradientBackground (g, getBounds (), START_COLOR, END_COLOR);

	    for(int i = 0; i < numImages; i++) {
		if(x[i] > 3*i) {
		    nudge(i);
		    squish(g, icon[i], xh[i], yh[i], scale[i]);
		} else {
		    x[i] += .05;
		    y[i] += .05;
		}
	    }
	}

	Random rand = new Random();

	public void nudge(int i) {
	    x[i] += (double) rand.nextInt(1000) / 8756;
	    y[i] += (double) rand.nextInt(1000) / 5432;
	    int tmpScale = (int) (Math.abs(Math.sin(x[i])) * 10);
	    scale[i] = (double) tmpScale / 10;
	    int nudgeX = (int) (((double) getWidth()/2) * .8);
	    int nudgeY = (int) (((double) getHeight()/2) * .60);
	    xh[i] = (int) (Math.sin(x[i]) * nudgeX) + nudgeX;
	    yh[i] = (int) (Math.sin(y[i]) * nudgeY) + nudgeY;
	}

	public void squish(Graphics g, ImageIcon icon, int x, int y, double scale) {
	    if(isVisible()) {
		g.drawImage(icon.getImage(), x, y,
			    (int) (icon.getIconWidth()*scale),
			    (int) (icon.getIconHeight()*scale),
			    this);
	    } 
	}
	
	public void actionPerformed(ActionEvent e) {
	    if(isVisible()) {
		repaint();
	    } else {
		animator.stop();
	    }
	}
    }


    private UIUtil () {}

    public static ImageIcon createImageIcon (String name) {
	return new ImageIcon (UIUtil.class.getResource(name));
    }

    // center component c on screen
    public static void centerComponent (Component c) {
	Dimension size = c.getSize();
	Dimension screenSize = c.getToolkit().getScreenSize();
        c.setLocation((screenSize.width  - size.width) / 2,
		      (int) ((screenSize.height - size.height) / 2));
    }

    // center component child respect to parent component
    public static void centerComponent (Component parent, Component child) {
	Dimension s0 = parent.getSize();
	Dimension s1 = child.getSize();
	child.setLocation(parent.getX() + (s0.width - s1.width)/2, 
			  parent.getY() + (s0.height - s1.height)/2);
    }

    public static void setupSplashScreen (URL imageURL) throws IOException {
	final SplashScreen splash = SplashScreen.getSplashScreen();
	if (splash == null) {
	    return;
	}
	splash.setImageURL(imageURL);
	
	final Graphics2D g = splash.createGraphics();
	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
			   RenderingHints.VALUE_ANTIALIAS_ON);
	g.setRenderingHint(RenderingHints.KEY_RENDERING, 
			   RenderingHints.VALUE_RENDER_QUALITY);
	
	final int size = 21;
	Rectangle r = splash.getBounds();
	g.translate(r.width - size - 5, r.height - size - 5);
	
	final BusyPainter painter = new BusyPainter
	    (new Rectangle2D.Float(0,0,5.5f,2.5f),
	     new Ellipse2D.Float(3.0f,3.0f,15.0f,15.0f));
	painter.setTrailLength(4);
	painter.setPoints(9);
	painter.setFrame(0);
	painter.setAntialiasing(true);
	
	final java.util.Timer timer = new java.util.Timer();
	timer.scheduleAtFixedRate
	    (new TimerTask () {
		    public void run () {
			if (splash.isVisible()) {
			    painter.setFrame
				((painter.getFrame()+1)%painter.getPoints());
			    painter.paint(g, null, size, size);
			    splash.update();
			}
			else {
			    timer.cancel();
			}
		    }
		}, 0, 100);
    }

    public static class BusyPane 
	extends JComponent implements ActionListener {

	javax.swing.Timer timer;
	BusyPainter painter;
	int frame = 0;
	AffineTransform txn = new AffineTransform ();
	int size = 14;
	int count = 0;

	public BusyPane () {
	    setPreferredSize (new Dimension (size+2, size+2));

	    painter = new BusyPainter
		(new RoundRectangle2D.Float(0.f, 0.f,5.f, 2.f, 4.f, 4.f),
		 new Ellipse2D.Float(2.f,2.f,10.0f,10.0f));
	    painter.setTrailLength(6);
	    painter.setPoints(9);
	    painter.setFrame(0);

	    timer = new javax.swing.Timer (125, this);
	}

	public void start () { 
	    if (count++ == 0) {
		timer.start(); 
	    }
	}
	public void stop () { 
	    if (--count == 0) {
		timer.stop(); 
		repaint (); 
	    }
	}
	public void reset () { count = 0; }

	public boolean isBusy () { return timer.isRunning(); }

	@Override
	protected void paintComponent (Graphics g) {
	    if (isBusy ()) {
		Graphics2D g2 = (Graphics2D)g;
		Rectangle clip = g.getClipBounds();
		
		Rectangle r = getBounds ();
		if (r != null && clip.intersects(r)) {
		    int x = r.x + (clip.width - size);
		    int y = r.y + (r.height - size)/2;
		    g2.translate(x, y);
		    painter.paint(g2, this, size, size);
		}
	    }
	}

	public void actionPerformed (ActionEvent e) {
	    frame = (frame+1) % 8;
	    painter.setFrame(frame);
	    repaint ();
	}
    }

    public static BusyPane createBusyPane () {
	return new BusyPane ();
    }

    public static void main (String[] argv) throws Exception {
	JFrame frame = new JFrame ();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	BouncingHeads bh = new BouncingHeads ();
	bh.go();
	frame.getContentPane().add(bh);
	frame.pack();
	frame.setSize(800, 600);
	frame.setVisible(true);
    }
}
