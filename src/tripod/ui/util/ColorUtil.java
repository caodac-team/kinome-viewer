// $Id$

package tripod.ui.util;

import java.awt.Color;

public class ColorUtil {
    public static final Color LightShadow = new Color (0, 0, 0, 125);
    public static final Color DarkShadow = new Color (0, 0, 0, 50);
    public static final Color Default = new Color (100, 150, 250);
    public static final Color Highlighter = new Color (255, 128, 243);
    public static final Color ForegroundXRef = new Color (0x606e80);
    public static final Color EvenRow = new Color (241, 245, 250);
    public static final Color OddRow = Color.white;
    public static final Color PaleYellow = new Color (242, 241, 234);
    public static final Color ItunesFontColor = new Color (0x606e80);

    private ColorUtil () {
    }
}
