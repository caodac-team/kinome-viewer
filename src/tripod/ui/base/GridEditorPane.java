package tripod.ui.base;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.HierarchyEvent;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import tripod.ui.util.ColorUtil;

public class GridEditorPane extends JPanel 
    implements ComponentListener, HierarchyListener {

    public GridEditorPane () {
	super (new BorderLayout ());
	setOpaque (false);
	setBackground (Color.white);
	setBorder (BorderFactory.createEmptyBorder(10,10,10,10));
	addComponentListener (this);
	addHierarchyListener (this);
    }
    
    protected void paintComponent (Graphics g) {
	Graphics2D g2 = (Graphics2D)g;
	g2.setRenderingHint(RenderingHints.KEY_RENDERING, 
			    RenderingHints.VALUE_RENDER_QUALITY);
	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
			    RenderingHints.VALUE_ANTIALIAS_ON);
	Rectangle r = getBounds ();
	int pad = (int)(Math.min(r.width, r.height)*.1 + 0.5);

	g2.setPaint(getBackground ());
	g2.setComposite(AlphaComposite.SrcAtop);
	    
	int shadow = getShadow (r.width, r.height);
	    
	g2.fillRoundRect(0, 0, r.width, r.height, r.width/3, r.height/3);
	//g2.setPaint(ColorUtil.Default);
	GradientPaint gp = new GradientPaint 
	    (0.f, 0.f, ColorUtil.Default.brighter(),
	     (float)r.width, (float)r.height, ColorUtil.Default);
	g2.setPaint(gp);
	g2.setStroke(new BasicStroke (2.f));
	g2.drawRoundRect
	    (0, 0, r.width-1, r.height-1, r.width/3, r.height/3);
	g2.setStroke(new BasicStroke (shadow));
	g2.drawRoundRect(shadow/2, shadow/2, 
			 r.width-shadow, r.height-shadow, 
			 r.width/3-shadow, r.height/3-shadow);
    }

    protected int getShadow (int width, int height) {
	int shadow = (int)(Math.min(width, height)*0.04+0.5);
	return Math.max(shadow, 2);
    }

    public void hierarchyChanged (HierarchyEvent e) {
	adjustBorder ();
    }

    public void componentHidden (ComponentEvent e) {
    }
    public void componentMoved (ComponentEvent e) {
    }
    public void componentResized (ComponentEvent e) {
	adjustBorder ();
    }
    public void componentShown (ComponentEvent e) {
	adjustBorder ();
    }

    protected void adjustBorder () {
	Rectangle r = getBounds ();
	int pad = (int)(Math.min(r.width, r.height)*.1 + 0.5);
	setBorder (BorderFactory.createEmptyBorder(pad,pad,pad,pad));
    }
}
