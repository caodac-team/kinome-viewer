// $Id: DefaultGridCellRenderer.java 3172 2009-08-18 20:28:10Z nguyenda $

package tripod.ui.base;

import java.awt.*;
import javax.swing.*;

public class DefaultGridCellRenderer 
    extends JLabel implements GridCellRenderer {

    public DefaultGridCellRenderer () {
	setOpaque (true);
	setHorizontalAlignment (SwingConstants.CENTER);
	setBackground (Color.white);
    }

    public Component getGridCellRendererComponent 
	(Grid g, Object value, boolean selected, int cell) {
	if (value == null) {
	    setText (null);
	}
	else {
	    setText (value.toString());
	}
	return this;
    }
}
