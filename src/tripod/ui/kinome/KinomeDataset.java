/*
                         PUBLIC DOMAIN NOTICE
                     NIH Chemical Genomics Center
               National Human Genome Research Institute

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software/database is freely
available to the public for use. The NIH Chemical Genomics Center
(NCGC) and the U.S. Government have not placed any restriction on its
use or reproduction. 

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.

*/
package tripod.ui.kinome;

import java.util.*;
import java.beans.*;
import java.awt.Rectangle;

public class KinomeDataset extends KinomeData implements Comparator<String> {
    private String name;
    private Map<String, Number> data = new TreeMap<String, Number>();
    private PropertyChangeSupport pcs = new PropertyChangeSupport (this);

    public KinomeDataset () {}
    public KinomeDataset (String name) {
	this.name = name;
    }

    public boolean isEmpty () { return data.isEmpty(); }
    public int size () { return data.size(); }
    public void clear () { 
	int size = data.size();
	data.clear(); 
	firePropertyChange ("size", size, 0);
    }
    public Map<String, Number> getValues () { 
	return Collections.unmodifiableMap(data); 
    }
    public boolean setValue (String kinase, Number value) {
        String uk = kinase.toUpperCase();
	boolean ok = KinaseBox.containsKey(uk);
	if (ok) {
	    Number old = data.put(uk, value);
	    firePropertyChange (uk, old, value);
	}
	return ok;
    }
    public Number getValue (String kinase) {
	return data.get(kinase.toUpperCase());
    }
    public Set<String> getKinases () { 
	Set<String> sorted = new TreeSet<String>(this);
	sorted.addAll(data.keySet());
	return sorted;
    }
    public boolean contains (String kinase) {
	return data.containsKey(kinase.toUpperCase());
    }
    public Set<String> intersects (KinomeDataset ks) {
	Set<String> kinases = new TreeSet<String>(data.keySet());
	kinases.retainAll(ks.data.keySet());
	return kinases;
    }

    public static Rectangle bbox (String kinase) {
	return KinaseBox.get(kinase.toUpperCase());
    }

    public static boolean isValidKinase (String kinase) {
	return KinaseBox.containsKey(kinase.toUpperCase());
    }

    // Comparator interface
    public int compare (String s1, String s2) {
	Number n1 = data.get(s1);
	Number n2 = data.get(s2);
	if (n1 != null && n2 != null) {
	    double d = n1.doubleValue() - n2.doubleValue();
	    if (d < 0.) return -1;
	    else if (d > 0.) return 1;
	}
	else if (n1 != null) {
	    return 1;
	}
	else if (n2 != null) {
	    return -1;
	}
            
	return s1.compareTo(s2);
    }

    public String getName () { return name; }
    public void setName (String name) { this.name = name; }

    public void addPropertyChangeListener (PropertyChangeListener l) {
	pcs.addPropertyChangeListener(l);
    }
    public void removePropertyChangeListener (PropertyChangeListener l) {
	pcs.removePropertyChangeListener(l);
    }
    public void firePropertyChange 
	(String name, Object oldValue, Object newValue) {
	pcs.firePropertyChange(name, oldValue, newValue);
    }
}
