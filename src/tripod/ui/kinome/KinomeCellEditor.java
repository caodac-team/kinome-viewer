
package tripod.ui.kinome;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.awt.Color;
import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import org.jdesktop.swingx.border.DropShadowBorder;
import tripod.ui.base.Grid;
import tripod.ui.base.GridCellEditor;
import tripod.ui.base.GridEditorPane;

public class KinomeCellEditor extends AbstractCellEditor 
    implements TableCellEditor, GridCellEditor {

    private static final Logger logger = 
	Logger.getLogger(KinomeCellEditor.class.getName());

    private KinomePanel kinome;
    private GridEditorPane gep;
    private Object value;
    
    public KinomeCellEditor () {
	KinomePanel kp = new KinomePanel ();
	kp.setBackground(Color.white);
	//kp.setPaintBorder(false);
	setKinomePanel (kp);
    }

    public KinomeCellEditor (KinomePanel kp) {
	setKinomePanel (kp);
    }

    public void setKinomePanel (KinomePanel kp) { kinome = kp; }
    public KinomePanel getKinomePanel () { return kinome; }

    public void setValue (Object value) {
	kinome.clear();

	if (value == null) {
	}
	else if (value instanceof KinomeDataset[]) {
	    KinomeDataset[] kds = (KinomeDataset[])value;
	    if (kds.length == 0) {
	    }
	    else if (kds.length == 1) {
		kinome.add(kds[0], KinomeCellRenderer.factories[0]);
	    }
	    else {
		for (int i = 0; i < kds.length; ++i) {
		    if (kds[i] != null) {
			kinome.add(kds[i], KinomeCellRenderer.factories
				   [1+(i%(KinomeCellRenderer.factories.length-1))]);
		    }
		}
	    }
	}
	else if (value instanceof KinomeDataset) {
	    kinome.add((KinomeDataset)value, 
		       KinomeCellRenderer.factories[0]);
	}
	this.value = value;
    }


    @Override
    public Object getCellEditorValue () {
	//return value;
	java.util.List<KinomeDataset> datasets = kinome.getDatasets();
	return datasets.isEmpty() 
	    ? null : datasets.toArray(new KinomeDataset[0]);
    }

    public Component getTableCellEditorComponent 
	(JTable table, Object value, boolean selected, int row, int column) {
	//logger.info("row="+row+" col="+column+": "+value);
	setValue (value);
	return kinome;
    }

    public Component getGridCellEditorComponent 
	(Grid g, Object value, boolean selected, int cell) {
	setValue (value);
	return getGridEditor ();
    }

    Component getGridEditor () {
	// lazy evaluation...
	if (gep == null) {
	    gep = new GridEditorPane ();
	    // force the border not to be painted
	    kinome.setPaintBorder(false);
	    gep.add(kinome);
	}
	return gep;
    }

}
