/*
                         PUBLIC DOMAIN NOTICE
                     NIH Chemical Genomics Center
               National Human Genome Research Institute

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software/database is freely
available to the public for use. The NIH Chemical Genomics Center
(NCGC) and the U.S. Government have not placed any restriction on its
use or reproduction. 

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.

*/
package tripod.ui.kinome;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.Icon;


public class DefaultKinomeRendererFactory implements KinomeRendererFactory {

    class DefaultItemRenderer implements KinomeItemRenderer, Icon {
	Shape item;
	DefaultItemRenderer (Shape item) {
	    this.item = item;
	}
	public Shape getShape () { return item; }
	public Color getColor () { return color; }
        public int getIconHeight () { return item.getBounds().height; }
        public int getIconWidth () { return item.getBounds().width; }
        public void paintIcon (Component c, Graphics g, int x, int y) {
            Graphics2D g2 = (Graphics2D)g;
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, 
                                RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
                                RenderingHints.VALUE_ANTIALIAS_ON);
	    g2.translate(x, y);
	    GradientPaint gp = new GradientPaint (0.f, 0.f, c.getBackground(),
						  (float)getIconWidth(),
						  (float)getIconHeight(),
						  color);
	    g2.setPaint(gp);
	    g2.fill(item);
	    g2.translate(-x, -y);
        }
    }
    
    protected Color color = Color.red;
    protected DefaultItemRenderer large = new DefaultItemRenderer
	(new Ellipse2D.Float(0.f, 0.f, 30.f, 30.f));
    protected DefaultItemRenderer medium = new DefaultItemRenderer
	(new Ellipse2D.Float(0.f, 0.f, 20.f, 20.f));
    protected DefaultItemRenderer small = new DefaultItemRenderer
	(new Ellipse2D.Float(0.f, 0.f, 10.f, 10.f));
    protected DefaultItemRenderer tiny = new DefaultItemRenderer
	(new Ellipse2D.Float(0.f, 0.f, 5.f, 5.f));
    
    public DefaultKinomeRendererFactory () {}
    public DefaultKinomeRendererFactory (Color color) {
	this.color = color;
    }

    public Color getColor () { return color; }
    public void setColor (Color color) { this.color = color; }
    
    public KinomeItemRenderer getRenderer (String kinase, Number value) {
	double x = value.doubleValue();
	
	if (x < 100.) {
	    return large;
	}
	else if (x < 500.) {
	    return medium;
	}
	else if (x < 1000.) {
	    return small;
	}
	return tiny;
    }

    public Icon tinyIcon () { return tiny; }
    public Icon smallIcon () { return small; }
    public Icon mediumIcon () { return medium; }
    public Icon largeIcon () { return large; }
}
