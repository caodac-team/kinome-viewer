package tripod.ui.kinome;

import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import java.awt.geom.*;
import java.io.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

public class KinomeImageAnnotator extends JPanel 
    implements MouseListener, MouseMotionListener {
    private static final Logger logger = Logger.getLogger
	(KinomeImageAnnotator.class.getName());

    private BufferedImage image;

    private Point start = null, end = null; // rubberband
    private Rectangle rubberband = new Rectangle ();
    private Point pointer;
    private ArrayList<Cell> cells = new ArrayList<Cell>();
    private ArrayList<Cell> selections = new ArrayList<Cell>();

    private JPopupMenu popup;
    private CellDialog dialog = null;

    enum Mode {
	DRAWING, SELECTION, UNSELECT
    }
    private Mode mode = Mode.DRAWING;
    private boolean showBox = true;

    private JFileChooser chooser = new JFileChooser (".");
    private File file;
    private long lastChanged = 0;
    private File imageRef;

    static class Cell {
	String name;
	Rectangle bbox;

	Cell (String name, Rectangle b) {
	    this.name = name;
	    this.bbox = new Rectangle (b.x, b.y, b.width, b.height);
	}

	public String toString () {
	    return name+"{x="+bbox.x+",y="+bbox.y+",w="
		+bbox.width+",h="+bbox.height+"}";
	}
    }

    class CellDialog extends JDialog implements ActionListener {
	JTextField field;

	CellDialog (JFrame parent) {
	    super (parent, true);
	    setTitle ("New cell");
	    setDefaultCloseOperation (DO_NOTHING_ON_CLOSE);

	    Box vbox = Box.createVerticalBox();
	    Box box = Box.createHorizontalBox();
	    
	    box.add(new JLabel ("Enter label", JLabel.LEADING));
	    box.add(Box.createHorizontalGlue());
	    vbox.add(box);
	    vbox.add(Box.createVerticalStrut(2));
	    vbox.add(field = new JTextField ());
	    field.addActionListener(this);
	    vbox.add(Box.createVerticalStrut(2));
	    JPanel bp = new JPanel (new GridLayout (1, 2, 5, 0));
	    JButton btn;
	    bp.add(btn = new JButton ("OK"));
	    btn.addActionListener(this);
	    bp.add(btn = new JButton ("Cancel"));
	    btn.addActionListener(this);
	    JPanel bbp = new JPanel ();
	    bbp.add(bp);
	    vbox.add(bbp);
	    vbox.add(Box.createVerticalGlue());

	    JPanel panel = new JPanel (new BorderLayout (0, 2));
	    panel.add(vbox, BorderLayout.NORTH);
	    getContentPane().add(panel);
	    pack ();
	}

	public void actionPerformed (ActionEvent e) {
	    String cmd = e.getActionCommand();
	    if (cmd.equalsIgnoreCase("cancel")) {
		field.setText(null);
	    }
	    setVisible (false);
	}

	void clear () { 
	    field.setText(null);
	}

	public String getInputValue () {
	    clear ();
	    setVisible (true);
	    String label = field.getText();
	    if (label == null || label.equals("")) {
		return null;
	    }
	    return label;
	}
    }

    public KinomeImageAnnotator () {
        initUI ();
        try {
            setImage (ImageIO.read(KinomeImageAnnotator.class.getResource
                                   ("resources/kinome_anno.png")));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public KinomeImageAnnotator (String file) {
        initUI ();
        try {
            setImage (ImageIO.read(imageRef = new File (file)));
        }
        catch (Exception ex) {
            throw new IllegalArgumentException (ex);
        }
    }

    void initUI () {
	popup = createPopup ();
	addMouseListener (this);
	addMouseMotionListener (this);
    }

    void setImage (BufferedImage image) {
        this.image = image;
	setPreferredSize 
	    (new Dimension (image.getWidth(), image.getHeight()));
        repaint ();
    }

    protected Rectangle getRubberband () {
	if (start == null  || end == null) {
	    return null;
	}
	int x = Math.min(start.x, end.x);
	int y = Math.min(start.y, end.y);
	int w = Math.max(start.x, end.x) - x + 1;
	int h = Math.max(start.y, end.y) - y + 1;
	rubberband.x = x;
	rubberband.y = y;
	rubberband.width = w;
	rubberband.height = h;
	return rubberband;
    }

    JPopupMenu createPopup () {
	JPopupMenu popup = new JPopupMenu ();
	JMenuItem item;
	ButtonGroup bg = new ButtonGroup ();

	popup.add(item = new JMenuItem ("Load"));
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    int ans = chooser.showOpenDialog(KinomeImageAnnotator.this);
		    if (ans == JFileChooser.APPROVE_OPTION) {
			try {
			    File f = chooser.getSelectedFile();
			    load (f);
			    setFile (f);
			}
			catch (IOException ex) {
			    logger.log(Level.SEVERE, "Can't load file \""
				       +chooser.getSelectedFile()+"\"", ex);

			    JOptionPane.showMessageDialog
				(KinomeImageAnnotator.this, ex.getMessage(), "Error",
				 JOptionPane.ERROR_MESSAGE);
			}
		    }
		}
	    });
	popup.add(item = new JMenuItem ("Save"));
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    try {
			save ();
		    }
		    catch (IOException ex) {
			logger.log(Level.SEVERE, "Can't save file \""
				   +file+"\"", ex);
			
			JOptionPane.showMessageDialog
			    (KinomeImageAnnotator.this, ex.getMessage(), "Error",
			     JOptionPane.ERROR_MESSAGE);
		    }
		}
	    });
	popup.add(item = new JMenuItem ("Save As..."));
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    int ans = chooser.showSaveDialog(KinomeImageAnnotator.this);
		    if (ans == JFileChooser.APPROVE_OPTION) {
			try {
			    File f = chooser.getSelectedFile();
			    saveAs (f);
			    setFile (f);
			}
			catch (IOException ex) {
			    logger.log(Level.SEVERE, "Can't save file \""
				       +chooser.getSelectedFile()+"\"", ex);

			    JOptionPane.showMessageDialog
				(KinomeImageAnnotator.this, ex.getMessage(), "Error",
				 JOptionPane.ERROR_MESSAGE);
			}
		    }
		}
	    });

	popup.addSeparator();
	popup.add(item = new JRadioButtonMenuItem ("Drawing"));
	item.setSelected(true);
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    mode = Mode.DRAWING;
		}
	    });
	bg.add(item);
	popup.add(item = new JRadioButtonMenuItem ("Selection"));
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    mode = Mode.SELECTION;
		}
	    });
	bg.add(item);
	popup.add(item = new JRadioButtonMenuItem ("Unselect"));
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    mode = Mode.UNSELECT;
		}
	    });
	bg.add(item);

	popup.addSeparator();
	popup.add(item = new JCheckBoxMenuItem ("Show box"));
	item.setSelected(true);
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    JCheckBoxMenuItem cb = (JCheckBoxMenuItem)e.getSource();
		    showBox = cb.isSelected();
		    repaint ();
		}
	    });

	popup.addSeparator();
	popup.add(item = new JMenuItem ("Delete"));
	item.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    for (Cell c : selections) {
			cells.remove(c);
			logger.info("deleting "+c);
			lastChanged = new java.util.Date().getTime();
		    }
		    selections.clear();
		    repaint ();
		}
	    });
	return popup;
    }

    public void load (File file) throws IOException {
	load (new FileInputStream (file));
    }

    public void load (InputStream is) throws IOException {
	BufferedReader br = new BufferedReader 
	    (new InputStreamReader (is));

	cells.clear();
	for (String line; (line = br.readLine()) != null; ) {
	    if (line.charAt(0) == '#') {
		continue;
	    }
	    String[] toks = line.split("\t");
	    if (toks.length == 2) {
		String[] coords = toks[0].split("[\\s]+");
		String[] names = toks[1].split("[\\s]+");
		for (String n : names) {
		    Rectangle r = new Rectangle (Integer.parseInt(coords[0]),
						 Integer.parseInt(coords[1]),
						 Integer.parseInt(coords[2]),
						 Integer.parseInt(coords[3]));
		    cells.add(new Cell (n, r));
		}
	    }
	}
	br.close();
	repaint ();

	logger.info(cells.size() + " cell(s) loaded!");
    }

    void setFile (File file) {
	this.file = file;
	Container c = getParent ();
	while (c != null && !(c instanceof JFrame)) {
	    c = c.getParent();
	}
	if (c != null) {
	    ((JFrame)c).setTitle("KinomeImageAnnotator \u2014 "+file.getName());
	}
    }

    public void save () throws IOException {
	if (file != null) {
	    save (new FileOutputStream (file));
	}
    }

    public void saveAs (File file) throws IOException {
	if (file.exists()) {
	    int ans = JOptionPane.showConfirmDialog
		(this, "File \""+file.getName()+"\" exists; overwrite?", 
		 "Confirmation", JOptionPane.YES_NO_OPTION);
	    if (ans == JOptionPane.YES_OPTION) {
		save (new FileOutputStream (file));		
	    }
	    else {
		logger.info("Save canceled!");
	    }
	}
	else {
	    save (new FileOutputStream (file));
	}
    }

    public void save (OutputStream os) throws IOException {
	PrintStream ps = new PrintStream (os);
	ps.println("# "+cells.size()+" cells for "+imageRef);
	for (Cell c : cells) {
	    ps.println(c.bbox.x+" "+c.bbox.y+" "+c.bbox.width+" "
		       +c.bbox.height+"\t"+c.name);
	}
	ps.flush();

	logger.info(cells.size() + " entries saved!");
    }

    @Override
    protected void paintComponent (Graphics g) {
	Graphics2D g2 = (Graphics2D)g;
	g2.setRenderingHint(RenderingHints.KEY_RENDERING, 
			    RenderingHints.VALUE_RENDER_QUALITY);
	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
			    RenderingHints.VALUE_ANTIALIAS_ON);
	g2.drawImage(image, 0, 0, null);

	Color color = g2.getColor();
	if (showBox) {
	    g2.setColor(Color.blue);
	    for (Cell c : cells) {
		if (pointer != null && c.bbox.contains(pointer)) {
		    g2.setColor(Color.red);
		    g2.draw(c.bbox);
		    g2.setColor(Color.blue);
		}
		else {
		    g2.draw(c.bbox);
		}
	    }
	    
	    g2.setColor(Color.red);
	    for (Cell c : selections) {
		g2.draw(c.bbox);
	    }
	}
	else {
	    g2.setColor(Color.red);
	    for (Cell c : cells) {
		if (pointer != null && c.bbox.contains(pointer)) {
		    g2.draw(c.bbox);
		}
	    }
	}
	g2.setColor(color);

	Rectangle rb = getRubberband ();
	if (rb != null) {
	    g2.setXORMode(Color.white);
	    g2.draw(rb);

	    g2.setColor(getBackground ());
	    g2.setComposite(AlphaComposite.getInstance
			    (AlphaComposite.SRC_OVER, 0.5f));
	    g2.fill(rb);
	}
    }


    public void mouseClicked (MouseEvent e) {
	clickGesture (e);
    }
    public void mouseEntered (MouseEvent e) {
    }
    public void mouseExited (MouseEvent e) {
    }
    public void mousePressed (MouseEvent e) {
	clickGesture (e);
    }
    public void mouseReleased (MouseEvent e) {
	Rectangle rb = getRubberband ();
	if (rb != null) {
	    if (dialog == null) {
		Container c = getParent ();
		while (!(c instanceof JFrame)) {
		    c = c.getParent();
		}
		dialog = new CellDialog ((JFrame)c);
	    }
	    String label = dialog.getInputValue();
	    if (label != null) {
		cells.add(new Cell (label, rb));
		lastChanged = new java.util.Date().getTime();
	    }
	}

	start = null;
	end = null;
	repaint ();
    }

    public void mouseDragged (MouseEvent e) {
	if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) 
	    == MouseEvent.BUTTON1_DOWN_MASK) {
	    end = e.getPoint();
	    repaint ();
	}
    }

    public void mouseMoved (MouseEvent e) {
	Cell cell = null;
	for (Cell c : cells) {
	    if (c.bbox.contains(e.getPoint())) {
		cell = c;
	    }
	}
	setToolTipText (cell != null ? cell.name : null);
	pointer = e.getPoint();
	repaint ();
    }

    boolean checkNeedSaving () {
	if (cells.isEmpty()) {
	    return false;
	}

	if (file == null || file.lastModified() < lastChanged) {
	    return true;
	}
	return false;
    }

    boolean promptSave (JFrame f) {
	int ans = JOptionPane.showConfirmDialog
	    (f, "Really exit without saving?", "Confirmation", 
	     JOptionPane.YES_NO_OPTION);
	if (ans == JOptionPane.YES_OPTION) {
	    return true;
	}
	return false;
    }

    boolean exit (JFrame f) {
	if (checkNeedSaving ()) {
	    return promptSave (f);
	}
	return true;
    }

    Cell getCell (Point pt) {
	for (Cell c : cells) {
	    if (c.bbox.contains(pt)) {
		return c;
	    }
	}
	return null;
    }

    void clickGesture (MouseEvent e) {
	if (e.isPopupTrigger()) {
	    popup.show(this, e.getX(), e.getY());
	}
	else {
	    start = e.getPoint();

	    switch (mode) {
	    case SELECTION:
		for (Cell c : cells) {
		    if (c.bbox.contains(start)) {
			selections.add(c);
			logger.info("selecting "+c);
		    }
		}
		repaint ();
		break;

	    case UNSELECT: 
		{
		    ArrayList<Cell> unselect = new ArrayList<Cell>();
		    for (Cell c : selections) {
			if (c.bbox.contains(start)) {
			    unselect.add(c);
			}
		    }
		    if (!unselect.isEmpty()) {
			for (Cell c : unselect) {
			    selections.remove(c);
			    logger.info("unselecting "+c);
			}
		    }
		}
		repaint ();
		break;
	    }
	}
    }

    public static void main (String[] argv) throws Exception {
	//System.setProperty("org.icepdf.core.scaleImages", "false");

        final KinomeImageAnnotator ki;
	if (argv.length == 0) {
            ki = new KinomeImageAnnotator ();
	}
        else {
            ki = new KinomeImageAnnotator (argv[0]);
        }

	JFrame frame = new JFrame (KinomeImageAnnotator.class.getName());
	//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	WindowAdapter adapter = new WindowAdapter () {
		public void windowClosing (WindowEvent e) {
		    final JFrame frame = (JFrame)e.getSource();
		    if (ki.exit(frame)) {
			System.exit(0);
		    }
		    SwingUtilities.invokeLater(new Runnable () {
			    public void run () {
				frame.setVisible(true);
			    }
			});
		}
	    };
	frame.addWindowListener(adapter);

	frame.getContentPane().add(new JScrollPane (ki));
	frame.pack();
	frame.setSize(800, 600);
	frame.setVisible(true);
    }
}
