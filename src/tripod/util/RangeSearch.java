// $Id$

package tripod.util;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Collection;

import java.awt.Shape;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;

import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.Lock;

/**
 * A not so efficient 2-d range search tree
 */
public class RangeSearch<V> {
    static class Interval implements Comparable<Interval> {
	double value, extent; 
	Interval (double value, double extent) {
	    this.value = value;
	    this.extent = extent;
	}

	public double getLeft () { return value; }
	public double getRight () { return value+extent; }

	public int compareTo (Interval n) {
	    if (getLeft() < n.getLeft()) return -1;
	    if (getLeft() > n.getLeft()) return 1;
	    if (getRight() < n.getRight()) return -1;
	    if (getRight() > n.getRight()) return 1;
	    return 0;
	}

	public boolean overlaps (Interval n) {
	    return getLeft() <= n.getRight() && getRight() >= n.getLeft();
	}
	public boolean overlaps (double v, double ext) {
	    return getLeft() <= (v+ext) && getRight() >= v;
	}

	public boolean contains (double v) {
	    return getLeft() <= v && getRight() >= v;
	}
    }

    private final SortedMap<Interval, SortedMap<Interval, V>> ranges = 
	new TreeMap<Interval, SortedMap<Interval, V>>();
    private int size = 0;

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock ();
    private final Lock readLock = lock.readLock();
    private final Lock writeLock = lock.writeLock();
    

    public RangeSearch () {
    }

    public void put (Shape key, V value) {
	if (key == null) {
	    return;
	}

	Rectangle2D r = key.getBounds2D();
	Interval x = new Interval (r.getX(), r.getWidth());
	Interval y = new Interval (r.getY(), r.getHeight());

	writeLock.lock();
	try {
	    SortedMap<Interval, V> slide = ranges.get(x);
	    if (slide == null) {
		ranges.put(x, slide = new TreeMap<Interval, V>());
	    }
	    
	    if (null == slide.put(y, value)) {
		++size;
	    }
	}
	finally {
	    writeLock.unlock();
	}
    }

    public int size () { return size; }

    public void clear () {
	writeLock.lock();
	try {
	    ranges.clear();
	    size = 0;
	}
	finally {
	    writeLock.unlock();
	}
    }

    public boolean isEmpty () { 
	readLock.lock();
	try {
	    return ranges.isEmpty(); 
	}
	finally {
	    readLock.unlock();
	}
    }

    public Collection<V> findOverlaps (int x, int y) {
	return findOverlaps (new Point (x, y));
    }

    public Collection<V> findOverlaps (Point2D pt) {
	ArrayList<V> results = new ArrayList<V>();

	readLock.lock();
	try {
	    for (Map.Entry<Interval, SortedMap<Interval, V>> e : 
		     ranges.entrySet()) {
		Interval x = e.getKey();
		if (x.contains(pt.getX())) {
		    SortedMap<Interval, V> slide = e.getValue();
		    for (Map.Entry<Interval, V> ee : slide.entrySet()) {
			Interval y = ee.getKey();
			if (y.contains(pt.getY())) {
			    results.add(ee.getValue());
			}
			else if (y.getLeft() > pt.getY()) {
			    break;
			}
		    }
		}
		else if (x.getLeft() > pt.getX()) {
		    break;
		}
	    }
	}
	finally {
	    readLock.unlock();
	}

	return results;
    }

    public Collection<V> findOverlaps (Shape region) {
	ArrayList<V> results = new ArrayList<V>();

	readLock.lock();
	try {
	    Rectangle2D b = region.getBounds2D();
	    for (Map.Entry<Interval, SortedMap<Interval, V>> e : 
		     ranges.entrySet()) {
		Interval x = e.getKey();
		if (x.overlaps(b.getX(), b.getWidth())) {
		    SortedMap<Interval, V> slide = e.getValue();
		    for (Map.Entry<Interval, V> ee : slide.entrySet()) {
			Interval y = ee.getKey();
			if (y.overlaps(b.getY(), b.getHeight())) {
			    //&& region.intersets
			    results.add(ee.getValue());
			}
			else if (y.getLeft() > b.getMaxY()) {
			    break;
			}
		    }
		}
		else if (x.getLeft() > b.getMaxX()) {
		    break;
		}
	    }
	}
	finally {
	    readLock.unlock();
	}

	return results;
    }
}
